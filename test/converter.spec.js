const expect = require("chai").expect;
const converter = require("../src/converter");

describe("color Code converter", () => {
  describe("RGB to Hex conversion", () => {
    it("converts the basic colors", () => {
      const redHex = converter.rgbToHex(255, 0, 0); // ff0000
      const greenHex = converter.rgbToHex(0, 255, 0); // 00ff00
      const blueHex = converter.rgbToHex(0, 0, 255); // 0000ff

      expect(redHex).to.equal("ff0000");
      expect(greenHex).to.equal("00ff00");
      expect(blueHex).to.equal("0000ff");
    });
  });

  describe("Hex to RGB conversion", () => {
    it("converts the basic colors", () => {
      const hexRed = "ff0000";
      const hexGreen = "00ff00";
      const hexBlue = "0000ff";
      const convertedRed = converter.hexToRgb(hexRed); // ff0000 converts to "255, 0, 0"
      const convertedGreen = converter.hexToRgb(hexGreen); // 00ff00 converts to "0, 255 , 0"
      const convertedBlue = converter.hexToRgb(hexBlue); // 0000ff converts to "0, 0, 255"
      expect(convertedRed).to.equal("255, 0, 0");
      expect(convertedGreen).to.equal("0, 255, 0");
      expect(convertedBlue).to.equal("0, 0, 255");
    });
  });
});
