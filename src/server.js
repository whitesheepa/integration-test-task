const { request } = require("express");
const express = require("express");
const converter = require("./converter");
const app = express();
const port = 3060;

app.get("/", (req, res) => res.send("Hello"));

app.get("/rgb-to-hex", (req, res) => {
  const red = parseInt(req.query.red, 10);
  const green = parseInt(req.query.green, 10);
  const blue = parseInt(req.query.blue);
  res.send(String(converter.rgbToHex(red, green, blue)));
});

app.get("/hex-to-rgb", (req, res) => {
  const hex = req.query.hex;
  res.send(converter.hexToRgb(hex));
});

if (process.env.NODE_ENV === "test") {
  module.exports = app;
} else {
  module.exports = app;
  app.listen(port, () => console.log(`Server: localhost:${port}`));
}
