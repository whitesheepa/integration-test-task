/**
 *	Padding outputs 2 characters allways
 *	@param {string} hex one or two characters
 *	returns {string} hex with two characters
 */
const pad = (hex) => {
  return hex.length === 1 ? "0" + hex : hex;
};

module.exports = {
  /**
   *	Converts RGB to Hex string
   *	@param {number} red 0-255
   *	@param {number} green 0-255
   *	@param {number} blue 0-255
   *	@param {string} hex value
   */
  rgbToHex: (red, green, blue) => {
    const redHex = red.toString(16); // may return single char
    const greenHex = green.toString(16);
    const blueHex = blue.toString(16);
    return pad(redHex) + pad(greenHex) + pad(blueHex);
  },

  /**
   *	Function converting hex string to RGB string
   *	@param {string} hex value
   *	@return {string} rgb
   */
  hexToRgb: (hex) => {
    var validHEXInput = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    if (!validHEXInput) {
      return false;
    }
    let r = parseInt(validHEXInput[1], 16);
    let g = parseInt(validHEXInput[2], 16);
    let b = parseInt(validHEXInput[3], 16);

    return `${r}, ${g}, ${b}`;
  },
};
